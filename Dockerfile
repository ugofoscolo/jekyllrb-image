FROM ruby:3-alpine3.12

RUN apk add build-base nodejs
RUN apk add --upgrade libxslt-dev
# RUN rm -rvf /var/cache/apk/*

RUN gem install pygments.rb
RUN gem install bundler:2.1.4
RUN gem update --system
RUN gem install jekyll:4.2.0

COPY Gemfile Gemfile

RUN bundle config set --local path 'vendor'
RUN bundle install
